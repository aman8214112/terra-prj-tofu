terraform {
  backend "s3" {
    bucket = "terra-prj-tofu"
    key    = "state"
    region = "eu-central-1"
  }
}
