#ec2

resource "aws_instance" "server" {
  ami             = "ami-0f7204385566b32d0"
  instance_type   = "m5.large"
  subnet_id       = var.sn
  security_groups = [var.sg]

  tags = {
    Name = "myserver"
  }
}